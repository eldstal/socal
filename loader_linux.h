#include <unistd.h>

// Load a library and return its base address
size_t load_lib(const char* libname);

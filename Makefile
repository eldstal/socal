all: test_wrapper.bin

#CXX=afl-g++
CXX=g++

%.bin: %.cpp loader_linux.cpp
	$(CXX) -fPIC -o $@ $^ -ldl


clean:
	-rm *.bin

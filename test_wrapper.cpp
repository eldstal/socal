// Library loader
#include "loader_linux.h"

// Hand-crafted function mapping for this test.
// Write a script to generate a similar header file for your 
// binary of interest!
#include "testlib/mapping.h"

#include <stdio.h>
#include <fcntl.h>
#include <malloc.h>

void* read_stdin_to_buf(size_t* total_size) {
	size_t bufsize = 0;
	size_t nread = 0;

	unsigned char* buf = NULL;

	while (true) {
		if ((nread + 1024) > bufsize) {
			bufsize += 1024;
			buf = (unsigned char*) realloc(buf, bufsize);
			//printf("Reallocated to %lu\n", bufsize);
		}
		size_t n = fread(&buf[nread], 1, 1024, stdin);
		nread += n;

		if (feof(stdin)) {
			break;
		}
	}

	*total_size = nread;
	return buf;
}


int main(int argc, char** argv) {

	size_t offset = load_lib("./testlib/testlib.so");
  if (offset == 0) {
    printf("Failed to load the target library.\n");
    return 1;
  }

  // Set up the function pointers into the loaded library
	TestLib lib(offset);

  // Call the function under test
	printf("do_some_math(20,30) = %d\n", lib.do_some_math(20, 30));

  // If provided on the command line, read stdin and pass it to our vulnerable function
  // Fuzz this, if you want!
  printf("Please provide input for vulnerable():\n");
  size_t bufsize;
  char* buf = (char*) read_stdin_to_buf(&bufsize);
  buf[bufsize-1] = 0;
  lib.vulnerable(buf);

  free(buf);
}

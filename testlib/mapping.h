#include <unistd.h>

class TestLib {
  public:

  // Offsets hand-plucked from IDA.
  // Function signatures guessed or whatever.
  void* (*get_an_offset)() = (void* (*)() ) 0x702;

  int (*do_some_math)(int,int) = (int (*)(int,int)) 0x6EA;

  void (*vulnerable)(char*) = (void (*)(char*)) 0x70F;

  // Apply the actual loaded library's base address at runtime
  TestLib(size_t offset) {

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wpointer-arith"

    get_an_offset += offset;
    do_some_math += offset;
    vulnerable += offset;

#pragma GCC diagnostic pop

  }

};


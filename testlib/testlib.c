#include <stdio.h>
#include <string.h>

int do_some_math(int a, int b) {
  return a*b + 256;
}

void* get_an_offset() {
  return &do_some_math;
}

void vulnerable(char* input) {
  char buf[256];
  strcpy(buf, input);
}

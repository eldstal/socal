#include <link.h>

#include <stdio.h>
#include <libgen.h>
#include <string.h>


struct LibSearch {
  size_t offset;
  const char* filename;
  void* handle;   // from dlopen()
};


static int
callback(struct dl_phdr_info *info, size_t size, void *data) {
  LibSearch* search = (LibSearch*) data;

  int j;
  const char *base = (const char *)info->dlpi_addr;
  const ElfW(Phdr) *first_load = NULL;

  //printf("Looking for %s. Is it %s?\n", search->filename, info->dlpi_name);
  if (strstr(info->dlpi_name, search->filename) == NULL) return 0;

  for (j = 0; j < info->dlpi_phnum; j++) {
    const ElfW(Phdr) *phdr = &info->dlpi_phdr[j];

    if (phdr->p_type != PT_LOAD) continue;
    if (first_load == NULL) first_load = phdr;

    if (!(phdr->p_flags & PF_X)) continue;

    //printf("ELF header is at %p, image linked at 0x%zx, relocation: 0x%zx\n",
    //       base + first_load->p_vaddr, first_load->p_vaddr, info->dlpi_addr);
    search->offset = (size_t) base + first_load->p_vaddr;
    return 1;
  }
  return 0;
}


size_t load_lib(const char* libname) {
  LibSearch search;
  search.offset = 0;
  search.handle = dlopen(libname, RTLD_NOW);

  struct link_map* lminfo;
  dlinfo(search.handle, RTLD_DI_LINKMAP, &lminfo);

  search.filename = basename(lminfo->l_name);
  dl_iterate_phdr(callback, &search);

  printf("Loaded library %s at offset 0x%.16lX\n", search.filename, search.offset);
  return search.offset;
}

# Socal - call stuff inside shared libraries

# Requirements
1. An .so library file of interest. It can be anything.
2. A list of addresses for interesting functions in that library. Don't worry about ASLR, just find their offsets in `.text` using objdump or ghidra or whatever
3. A header file (see `testlib/mapping.h` for an example) creating function pointers using those addresses and the proper type signature
4. A function to apply a fixed offset (the location where the library was loaded at runtime) to those function pointers

# Instructions
1. Create a wrapper program (see `test_wrapper.cpp` for an example) which links against libdl.
2. Include your header file with the function mappings
3. Call `load_library` from `linux_loader.cpp` to load your .so and learn its load address
4. Apply the load address as an offset to all your function pointers
5. Call the functions at your leisure


# TODO
* A script to auto-generate the function mapping header.
* Maybe use gdb debug symbols or DWARF info or whatever else is in the library already?
